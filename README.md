### Installation
```
sudo apt update \
    && sudo apt install -y ansible sshpass
```
```
export ANSIBLE_HOST_KEY_CHECKING=False
```

### Configure
```
ansible-playbook kubernetes.yml --ask-pass --ask-become-pass
```

### Ping
```
ansible all -m ping --ask-pass
```

### Usage
*Try it now!*
